import os
import yaml

information = [
    'title',
    'on_arxiv',
    'abstract'
]
publist_to_md_entries = {
    'title': 'title',
    'on_arxiv': 'date',
    'abstract': 'abstract'
}

os.system('rm -rf ./en/publication/* ./pt/publication/*')

with open('./publications.yml') as f:
    pub_list = yaml.load(f, Loader=yaml.FullLoader)

pubdir = './en/publication/'
# os.system('rm -rf ' + pubdir + '1* ' + pubdir + '2* ' + pubdir + 'MgB2_wire ' + pubdir + 'Stearic_acid_MgB2')

for entry in pub_list:
    filedir = pubdir + entry['id']
    os.mkdir(filedir)
    filenm = filedir + '/index.md'
    with open(filenm, 'w', encoding='utf8') as the_file:
        the_file.write('---\n')
        for info in information:
            if entry[info]:
                the_file.write(publist_to_md_entries[info] + ': "' + str(entry[info]) + '"\n')
        if 'doi' in entry:
            the_file.write('doi: "' + str(entry['doi']) + '"\n')
        if 'journal' in entry:
            the_file.write('publication: "' + str(entry['journal']) + '"\n')
        the_file.write('authors: [')
        for author in entry['authors']:
            the_file.write('"' + author + '", ')
        the_file.write(']\n')
        the_file.write('links:\n')
        if 'journal_url' in entry:
            the_file.writelines(
                [
                    '- name: Journal link\n',
                    '  url: ' + entry['journal_url'] + '\n',
                ]
            )
        if 'youtube_id' in entry:
            the_file.writelines(
                [
                    '- name: Online talk\n',
                    '  url: http://youtu.be/' + entry['youtube_id'] + '\n',
                ]
            )
        if 'no_preprint' in entry:
            pass
        else:
            the_file.writelines(
                [
                    '- name: arXiv link\n',
                    '  url: https://arxiv.org/abs/' + entry['id'] + '\n'
                ]
            )
        the_file.write('publication_types: ["2"]\n')
        the_file.write('math: true\n')
        the_file.write('highlight: true\n')
        the_file.write('---\n')

os.system('cp -r ./en/publication/* ./pt/publication')