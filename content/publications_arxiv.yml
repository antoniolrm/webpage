- abstract: Kondo physics and heavy-fermion behavior have been predicted and observed
    in moir\'e materials. The electric tunability of moir\'e materials allows an in-situ
    study of Kondo lattices' phase diagrams, which is not possible with their intermetallic
    counterparts. However, moir\'e platforms rely on twisting, which introduces twisting
    angle disorder and undesired buckling. Here we propose device layouts for one-
    and two-dimensional gate-defined superlattices in Bernal bilayer graphene where
    localized states couple to dispersive valley-helical modes. We show that, under
    electronic interactions, these superlattices are described by an electrically
    tunable Kondo-Heisenberg model.
  authors:
  - Antonio L. R. Manesco
  id: '2408.07148'
  on_arxiv: 2024-08-13
  title: Gate-defined Kondo lattices with valley-helical quantum dot arrays
- abstract: 'Despite many reports of valley-related phenomena in graphene and its
    multilayers, current transport experiments cannot probe valley phenomena without
    the application of external fields. Here we propose a gate-defined valley splitter
    as a direct transport probe for valley phenomenon in graphene multilayers. First,
    we show how the device works, its magnetotransport response, and its robustness
    against fabrication errors. Secondly, we present two applications for valley splitters:
    (i) resonant tunneling of quantum dots probed by a valley splitter shows the valley
    polarization of dot levels; (ii) a combination of two valley splitters resolves
    the nature of order parameters in mesoscopic samples.'
  authors:
  - Juan Daniel Torres Luna
  - Kostas Vilkelis
  - Antonio L. R. Manesco
  id: '2405.00538'
  on_arxiv: 2024-05-01
  title: Probing valley phenomena with gate-defined valley splitters
  youtube_id: 424oTxtFQNU
- abstract: We propose a practical implementation of a universal quantum computer
    that uses local fermionic modes (LFM) rather than qubits. The device layout consists
    of quantum dots tunnel coupled by a hybrid superconducting island and a tunable
    capacitive coupling between the dots. We show that coherent control of Cooper
    pair splitting, elastic cotunneling, and Coulomb interactions allows us to implement
    the universal set of quantum gates defined by Bravyi and Kitaev. Due to the similarity
    with charge qubits, we expect charge noise to be the main source of decoherence.
    For this reason, we also consider an alternative design where the quantum dots
    have tunable coupling to the superconductor. In this second device design, we
    show that there is a sweetspot for which the local fermionic modes are charge
    neutral, making the device insensitive to charge noise effects. Finally, we compare
    both designs and their experimental limitations and suggest future efforts to
    overcome them.
  authors:
  - Kostas Vilkelis
  - Antonio Manesco
  - Juan Daniel Torres Luna
  - Sebastian Miles
  - Michael Wimmer
  - Anton Akhmerov
  doi: 10.21468/SciPostPhys.16.5.135
  id: '2309.00447'
  journal: SciPost Phys.
  journal_url: https://doi.org/10.21468/scipostphys.16.5.135
  on_arxiv: 2023-09-01
  page: '135'
  published: 2024-05-27
  title: Fermionic quantum computation with Cooper pair splitters
  vol: '16'
  youtube_id: _uUHnu1Dec0
- abstract: We demonstrate that Andreev modes that propagate along a transparent Josephson
    junction have a perfect transmission at the point where three junctions meet.
    The chirality and the number of quantized transmission channels is determined
    by the topology of the Fermi surface and the vorticity of the superconducting
    phase differences at the trijunction. We explain this chiral adiabatic transmission
    (CAT) as a consequence of the adiabatic evolution of the scattering modes both
    in momentum and real space. We identify an effective energy barrier that guarantees
    quantized transmission. We expect that CAT is observable in nonlocal conductance
    and thermal transport measurements. Furthermore, because it does not rely on particle-hole
    symmetry, CAT is also possible to observe directly in metamaterials.
  authors:
  - Isidora Araya Day
  - Kostas Vilkelis
  - Antonio L. R. Manesco
  - A. Mert Bozkurt
  - Valla Fatemi
  - Anton R. Akhmerov
  id: '2311.17160'
  on_arxiv: 2023-11-28
  title: Chiral adiabatic transmission protected by Fermi surface topology
  youtube_id: OFBGtmzKjuI
- abstract: The achievement of valley-polarized electron currents is a cornerstone
    for the realization of valleytronic devices. Here, we report on ballistic coherent
    transport experiments where two opposite quantum point contacts (QPCs) are defined
    by electrostatic gating in a bilayer graphene (BLG) channel. By steering the ballistic
    currents with an out-of-plane magnetic field we observe two current jets, a consequence
    of valley-dependent trigonal warping. Tuning the BLG carrier density and number
    of QPC modes (m) with a gate voltage we find that the two jets are present for
    m=1 and up to m=6, indicating the robustness of the effect. Semiclassical simulations
    which account for size quantization and trigonal warping of the Fermi surface
    quantitatively reproduce our data without fitting parameters, confirming the origin
    of the signals. In addition, our model shows that the ballistic currents collected
    for non-zero magnetic fields are valley-polarized independently of m, but their
    polarization depends on the magnetic field sign, envisioning such devices as ballistic
    current sources with tuneable valley-polarization.
  authors:
  - "Josep Ingla-Ayn\xE9s"
  - Antonio L. R. Manesco
  - Talieh S. Ghiasi
  - Kenji Watanabe
  - Takashi Taniguchi
  - Herre S. J. van der Zant
  id: '2310.15293'
  on_arxiv: 2023-10-23
  title: A ballistic electron source with magnetically-controlled valley polarization
    in bilayer graphene
- abstract: Caroli-de Gennes-Matricon (CdGM) states are localized states with a discrete
    energy spectrum bound to the core of vortices in superconductors. In topological
    superconductors, CdGM states are predicted to coexist with zero-energy, chargeless
    states widely known as Majorana zero modes (MZMs). Due to their energy difference,
    current experiments rely on scanning tunneling spectroscopy methods to distinguish
    between them. This work shows that electrostatic inhomogeneities can push trivial
    CdGM states arbitrarily close to zero energy in non-topological systems where
    no MZM is present. Furthermore, the BCS charge of CdGM states is suppressed under
    the same mechanism. Through exploration of the impurity parameter space, we establish
    that these two phenomena generally happen in consonance. Our results show that
    energy and charge shifts in CdGM may be enough to imitate the spectroscopic signatures
    of MZMs even in cases where the estimated CdGM level spacing (in the absence of
    impurities) is much larger than the typical experimental level broadening.
  authors:
  - "Bruna S. de Mendon\xE7a"
  - Antonio L. R. Manesco
  - Nancy Sandler
  - Luis G. G. V. Dias da Silva
  doi: 10.1103/PhysRevB.107.184509
  id: '2204.05078'
  journal: Phys. Rev. B
  journal_url: https://doi.org/10.1103/physrevb.107.184509
  on_arxiv: 2022-04-11
  page: '184509'
  published: 2023-05-17
  title: Near zero-energy Caroli-de Gennes-Matricon vortex states in the presence
    of impurities
  vol: '107'
  youtube_id: WE_sxbXYdYM
- abstract: We theoretically predict spatial separation of spin-polarized ballistic
    currents in transition metal dichalcogenides (TMDs) due to trigonal warping. We
    quantify the effect in terms of spin polarization of charge carrier currents in
    a prototypical 3-terminal ballistic device where spin-up and spin-down charge
    carriers are collected by different leads. We show that the magnitude of the current
    spin polarization depends strongly on the charge carrier energy and the direction
    with respect to crystallographic orientations in the device. We study the (negative)
    effect of lattice imperfections and disorder on the observed spin polarization.
    Our investigation provides an avenue towards observing spin discrimination in
    a defect-free time reversal-invariant material.
  authors:
  - Antonio L. R. Manesco
  - Artem Pulkin
  doi: 10.21468/SciPostPhysCore.6.2.036
  id: '2206.07333'
  journal: SciPost Phys. Core
  journal_url: https://doi.org/10.21468/scipostphyscore.6.2.036
  on_arxiv: 2022-06-15
  page: '036'
  published: 2023-05-01
  title: Spatial separation of spin currents in transition metal dichalcogenides
  vol: '6'
- abstract: We report on multiterminal measurements in a ballistic bilayer graphene
    (BLG) channel where multiple spin and valley-degenerate quantum point contacts
    (QPCs) are defined by electrostatic gating. By patterning QPCs of different shapes
    and along different crystallographic directions, we study the effect of size quantization
    and trigonal warping on the transverse electron focusing (TEF) spectra. Our TEF
    spectra show eight clear peaks with comparable amplitude and weak signatures of
    quantum interference at the lowest temperature, indicating that reflections at
    the gate-defined edges are specular and transport is phase coherent. The temperature
    dependence of the scattering rate indicates that electron-electron interactions
    play a dominant role in the charge relaxation process for electron doping and
    temperatures below 100 K. The achievement of specular reflection, which is expected
    to preserve the pseudospin information of the electron jets, is promising for
    the realization of ballistic interconnects for new valleytronic devices.
  authors:
  - "Josep Ingla-Ayn\xE9s"
  - Antonio L. R. Manesco
  - Talieh S. Ghiasi
  - Serhii Volosheniuk
  - Kenji Watanabe
  - Takashi Taniguchi
  - Herre S. J. van der Zant
  doi: 10.1021/acs.nanolett.3c00499
  id: '2302.00303'
  journal: Nano Lett.
  journal_url: https://doi.org/10.1021/acs.nanolett.3c00499
  on_arxiv: 2023-02-01
  page: '5453'
  published: 2023-06-28
  title: Specular electron focusing between gate-defined quantum point contacts in
    bilayer graphene
  vol: '23'
- abstract: We simulate a hybrid superconductor-graphene device in the quantum Hall
    regime to identify the origin of downstream resistance oscillations in a recent
    experiment [Zhao et. al. Nature Physics 16, (2020)]. In addition to the previously
    studied Mach-Zehnder interference between the valley-polarized edge states, we
    consider disorder-induced scattering, and the previously overlooked appearance
    of the counter-propagating states generated by the interface density mismatch.
    Comparing our results with the experiment, we conclude that the observed oscillations
    are induced by the interfacial disorder, and that lattice-matched superconductors
    are necessary to observe the alternative ballistic effects.
  authors:
  - Antonio L. R. Manesco
  - "Ian Matthias Fl\xF3r"
  - Chun-Xiao Liu
  - Anton R. Akhmerov
  doi: 10.21468/SciPostPhysCore.5.3.045
  id: '2103.06722'
  journal: SciPost Phys. Core
  journal_url: https://doi.org/10.21468/scipostphyscore.5.3.045
  on_arxiv: 2021-03-11
  page: '045'
  published: 2022-09-27
  title: Mechanisms of Andreev reflection in quantum Hall graphene
  vol: '5'
  youtube_id: Sahg_wRv_kw
- abstract: Flat bands emerging in buckled monolayer graphene superlattices have been
    recently shown to realize correlated states analogous to those observed in twisted
    graphene multilayers. Here, we demonstrate the emergence of valley topology driven
    by competing electronic correlations in buckled graphene superlattices. We show,
    both by means of atomistic models and a low-energy description, that the existence
    of long-range electronic correlations leads to a competition between antiferromagnetic
    and charge density wave instabilities, that can be controlled by means of screening
    engineering. Interestingly, we find that the emergent charge density wave has
    a topologically non-trivial electronic structure, leading to a coexistent quantum
    valley Hall insulating state. In a similar fashion, the antiferromagnetic phase
    realizes a spin-polarized quantum valley-Hall insulating state. Our results put
    forward buckled graphene superlattices as a new platform to realize interaction-induced
    topological matter.
  authors:
  - Antonio L. R. Manesco
  - Jose L. Lado
  doi: 10.1088/2053-1583/ac0b48
  id: '2104.00573'
  journal: 2D Mater.
  journal_url: https://doi.org/10.1088/2053-1583/ac0b48
  on_arxiv: 2021-04-01
  page: '035057'
  published: 2021-07-01
  title: Correlation-induced valley topology in buckled graphene superlattices
  vol: '8'
  youtube_id: 92gahnZ920g
- abstract: In the present work, we investigated the electronic and elastic properties
    in equilibrium and under strain of the type-II Dirac semimetal NiTe$_2$ using
    density functional theory (DFT). Our results demonstrate the tunability of Dirac
    nodes' energy and momentum with strain and that it is possible to bring them closer
    to the Fermi level, while other metallic bands are supressed. We also derive a
    minimal 4-band effective model for the Dirac cones which accounts for the aforementioned
    strain effects by means of lattice regularization, providing an inexpensive way
    for further theoretical investigations and easy comparison with experiments. On
    an equal footing, we propose the static control of the electronic structure by
    intercalating alkali species into the van der Waals gap, resulting in the same
    effects obtained by strain-engineering and removing the requirement of in situ
    strain. Finally, evaluating the wavefunction's symmetry evolution as the lattice
    is deformed, we discuss possible consequences, such as Liftshitz transitions and
    the coexistence of type-I and type-II Dirac cones, thus motivating future investigations.
  authors:
  - Pedro P. Ferreira
  - Antonio L. R. Manesco
  - Thiago T. Dorini
  - Lucas E. Correa
  - Gabrielle Weber
  - Antonio J. S. Machado
  - Luiz T. F. Eleno
  doi: 10.1103/PhysRevB.103.125134
  id: '2006.14071'
  journal: Phys. Rev. B
  journal_url: https://doi.org/10.1103/physrevb.103.125134
  on_arxiv: 2020-06-24
  page: '125134'
  published: 2021-03-15
  title: Strain-engineering the topological type-II Dirac semimetal NiTe$_2$
  vol: '103'
  youtube_id: 1ZCcnwqFZsM
- abstract: We study the electronic properties of InAs/EuS/Al heterostructures as
    explored in a recent experiment [S. Vaitiekenas \emph{et al.}, Nat. Phys. (2020)],
    combining both spectroscopic results and microscopic device simulations. In particular,
    we use angle-resolved photoemission spectroscopy to investigate the band bending
    at the InAs/EuS interface. The resulting band offset value serves as an essential
    input to subsequent microscopic device simulations, allowing us to map the electronic
    wave function distribution. We conclude that the magnetic proximity effects at
    the Al/EuS as well as the InAs/EuS interfaces are both essential to achieve topological
    superconductivity at zero applied magnetic field. Mapping the topological phase
    diagram as a function of gate voltages and proximity-induced exchange couplings,
    we show that the ferromagnetic hybrid nanowire with overlapping Al and EuS layers
    can become a topological superconductor within realistic parameter regimes, and
    that the topological phase can be optimized by external gating. Our work highlights
    the need for a combined experimental and theoretical effort for faithful device
    simulation.
  authors:
  - Chun-Xiao Liu
  - Sergej Schuwalow
  - Yu Liu
  - Kostas Vilkelis
  - A. L. R. Manesco
  - P. Krogstrup
  - Michael Wimmer
  doi: 10.1103/PhysRevB.104.014516
  id: '2011.06567'
  journal: Phys. Rev. B
  journal_url: https://doi.org/10.1103/physrevb.104.014516
  on_arxiv: 2020-11-12
  page: '014516'
  published: 2021-07-22
  title: Electronic properties of InAs/EuS/Al hybrid nanowires
  vol: '104'
  youtube_id: G2reZGMaN_Y
- abstract: Electronic correlations stemming from nearly flat bands in van der Waals
    materials have demonstrated to be a powerful playground to engineer artificial
    quantum matter, including superconductors, correlated insulators and topological
    matter. This phenomenology has been experimentally observed in a variety of twisted
    van der Waals materials, such as graphene and dichalcogenide multilayers. Here
    we show that spontaneously buckled graphene can yield a correlated state, emerging
    from an elastic pseudo Landau level. Our results build on top of recent experimental
    findings reporting that, when placed on top of hBN or NbSe$_2$ substrates, wrinkled
    graphene sheets relax forming a periodic, long-range buckling pattern. The low-energy
    physics can be accurately described by electrons in the presence of a pseudo-axial
    gauge field, leading to the formation of sublattice-polarized Landau levels. Moreover,
    we verify that the high density of states at the zeroth Landau level leads to
    the formation of a periodically modulated ferrimagnetic groundstate, which can
    be controlled by the application of external electric fields. Our results indicate
    that periodically strained graphene is a versatile platform to explore emergent
    electronic states arising from correlated elastic Landau levels.
  authors:
  - Antonio L. R. Manesco
  - Jose L. Lado
  - Eduardo V. S. Ribeiro
  - Gabrielle Weber
  - Durval Rodrigues Jr
  doi: 10.1088/2053-1583/abbc5f
  id: '2003.05163'
  journal: 2D Mater.
  journal_url: https://doi.org/10.1088/2053-1583/abbc5f
  on_arxiv: 2020-03-11
  page: '015011'
  published: 2021-01-01
  title: Correlations in the elastic Landau level of spontaneously buckled graphene
  vol: '8'
  youtube_id: xNxuybvuuHk
- abstract: It was recently proposed that the interface between a graphene nanoribbon
    in the canted antiferromagnetic quantum Hall state and a s-wave superconductor
    may present topological superconductivity, resulting in the appearance of Majorana
    zero modes. However, a description of the low-energy physics in terms of experimentally
    controllable parameters was still missing. Starting from a mean-field continuum
    model for graphene in proximity to a superconductor, we derive the low-energy
    effective Hamiltonian describing the interface of this heterojunction from first
    principles. A comparison between tight-binding simulations and analytical calculations
    with effective masses suggests that normal reflections at the interface must be
    considered in order to fully describe the low-energy physics.
  authors:
  - A. L. R. Manesco
  - D. Rodrigues Jr.
  - G. Weber
  doi: 10.1103/PhysRevB.100.125411
  id: '1903.00807'
  journal: Phys. Rev. B
  journal_url: https://doi.org/10.1103/physrevb.100.125411
  on_arxiv: 2019-03-03
  page: '125411'
  published: 2019-09-09
  title: Effective model for Majorana modes in graphene
  vol: '100'
- abstract: The physical properties of the Zr$_5$Pt$_3$ compound with interstitial
    carbon in hexagonal D8$_8$-structure was investigated. A set of macroscopic measurements
    reveal a bulk superconducting at approximately 7 K for Zr$_5$Pt$_3$C$_{0.3}$ close
    to Zr$_5$Pt$_3$, also with a correlate anomalous resistivity behavior. However,
    both the signatures of strong electron-electron interaction, and the electronic
    contribution to specific heat, increase dramatically with the C doping. For the
    first time the x-ray photoelectron spectra compared with DFT/PWLO calculations
    of electronic structure show a complex Fermi surface with high density of states
    for Zr$_5$Pt$_3$. Also results show the signature of unconventional superconductivity.
    Indeed, was observed an unusual behavior for lower and upper critical field diagrams
    of Zr$_5$Pt$_3$C$_{0.3}$. The temperature dependence of penetration length and
    electronic contribution to specific heat suggests that electronic pairing deviates
    of $s$-wave the BCS scenario.
  authors:
  - S. T. Renosto
  - R. Lang
  - A. L. R. Manesco
  - D. Rodrigues Jr.
  - F. B. Santos
  - A. J. S. Machado
  - M. R. Baldan
  - E. Diez
  id: '1809.05008'
  on_arxiv: 2018-09-13
  title: Strong Electronic Interaction and Signatures of Nodal Superconductivity in
    Zr$_5$Pt$_3$C$_x$
- abstract: Transition metal dichalcogenides (TMDs) usually show simple structures,
    however, with interesting properties. Recently some TMDs have been pointed out
    as type-II Dirac semimetals. In the present work, we investigate the physical
    properties of a new candidate for type-II Dirac semimetal and investigate the
    effect of titanium doping on physical properties of Ti-doped single crystalline
    samples of NiTe2. It was found that this compound shows a superconducting properties
    with a critical temperature close to 4.0 K. Interestingly, applied pressures up
    to 1.3 GPa have no effect upon the superconducting state. Density Functional Theory
    (DFT) calculations demonstrate the presence of a Dirac cone in the band structure
    of NiTe2 literature when Spin-Orbit Coupling (SOC) is included, which is in agreement
    with a recent report for this compound. Also, our calculations demonstrate that
    Ti suppresses the formation of these non-trivial states.
  authors:
  - B. S. de Lima
  - R. R. de Cassia
  - F. B. Santosa
  - L. E. Correa
  - T. W. Grant
  - A. L. R. Manesco
  - G. W. Martins L. T. F. Eleno
  - M. S. Torikachvili
  - A. J. S. Machado
  doi: 10.1016/j.ssc.2018.08.014
  id: '1809.00753'
  journal: Solid State Communications
  journal_url: https://doi.org/10.1016/j.ssc.2018.08.014
  on_arxiv: 2018-09-04
  page: '27'
  published: 2018-11-01
  title: Properties and superconductivity in Ti-doped NiTe2 single crystals
  vol: '283'
- abstract: Majorana fermions are particles identical to their antiparticles proposed
    theoretically in 1937 by Ettore Majorana as real solutions of the Dirac equation.
    Alexei Kitaev suggested that Majorana particles should emerge in condensed matter
    systems as zero mode excitations in one-dimensional p-wave superconductors, with
    possible applications in quantum computation due to their non-abelian statistics.
    The search for Majorana zero modes in condensed matter systems led to one of the
    first realistic models based in a semiconductor nanowire with high spin-orbit
    coupling, induced superconducting s-wave pairing and Zeeman splitting. Soon, it
    was realized that size-quantization effects should generate subbands in these
    systems that could even allow the emergence of more than one Majorana mode at
    each edge, resulting in a zero bias peak on the differential conductance with
    a different shape from the predicted by simplified theoretical models. In this
    work, we provide a connection between a finite-size nanowire with two occupied
    subbands and a 2-band Kitaev chain and discuss the advantage of an one-dimensional
    model to understand the phenomenology of the system, including the presence of
    a hidden chiral symmetry and its similarity with a spinfull Kitaev chain under
    a magnetic field.
  authors:
  - "Ant\xF4nio Lucas Rigotti Manesco"
  - Gabriel Weber
  - Durval Rodrigues Jr
  doi: 10.1109/TASC.2018.2807361
  id: '1710.06769'
  journal: IEEE Trans. Appl. Supercond.
  journal_url: https://doi.org/10.1109/tasc.2018.2807361
  on_arxiv: 2017-10-18
  page: '1'
  published: 2018-06-01
  title: One-dimensional p-wave superconductor toy-model for Majorana fermions in
    multiband semiconductor nanowires
  vol: '28'
- abstract: Realistic implementations of the Kitaev chain require, in general, the
    introduction of extra internal degrees of freedom. In the present work, we discuss
    the presence of hidden BDI symmetries for free Hamiltonians describing systems
    with an arbitrary number of internal degrees of freedom. We generalize results
    of a spinfull Kitaev chain to construct a Hamiltonian with $n$ internal degrees
    of freedom and obtain the corresponding hidden chiral symmetry. As an explicit
    application of this generalized result, we exploit by analytical and numerical
    calculations the case of a spinful 2-band Kitaev chain, which can host up to 4
    Majorana bound states. We also observe the appearence of minigap states, when
    chiral symmetry is broken.
  authors:
  - "Ant\xF4nio Lucas Rigotti Manesco"
  - Gabriel Weber
  - Durval Rodrigues Jr
  doi: 10.1088/1361-648X/aab722
  id: '1708.07866'
  journal: 'J. Phys.: Condens. Matter'
  journal_url: https://doi.org/10.1088/1361-648x/aab722
  on_arxiv: 2017-08-25
  page: '175401'
  published: 2018-05-02
  title: Hidden chiral symmetries in BDI multichannel Kitaev chains
  vol: '30'
