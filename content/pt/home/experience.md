+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "Experiência"
subtitle = ""

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Pesquisador de pós-doutorado"
  company = "Delft University of Technology"
  company_url = "https://www.tudelft.nl/"
  location = "Delft - Holanda"
  date_start = "2021-07-01"
  date_end = ""
  description = """
  Responsibilidades:

  * Pesquisa científica
  * Auxílio na supervisão de alunos
  """

[[experience]]
  title = "Pesquisador de doutorado"
  company = "Universidade de São Paulo"
  company_url = "https://www5.usp.br/"
  location = "Lorena - São Paulo - Brazil"
  date_start = "2016-08-01"
  date_end = "2021-06-01"
  description = """
  Responsibilidades:

  * Pesquisa científica
  * Auxílio na supervisão de projetos de iniciação científica
  * Estágio em docência
  """

[[experience]]
  title = "Estágio em pesquisa"
  company = "Delft University of Technology"
  company_url = "https://www.tudelft.nl/"
  location = "Delft - Netherlands"
  date_start = "2019-09-01"
  date_end = "2020-08-31"
  description = """
  Responsibilidades:

  * Pesquisa científica
  * Supervisão de projetos de conclusão de curso
  """

[[experience]]
  title = "Iniciação científica"
  company = "Universidade de São Paulo"
  company_url = "https://www5.usp.br/"
  location = "Lorena - São Paulo - Brazil"
  date_start = "2013-08-01"
  date_end = "2016-07-30"
  description = "Pesquisa científica"

+++
