---
title: "Strain-engineering the topological type-II Dirac semimetal NiTe$_2$"
date: "2020-06-24"
abstract: "In the present work, we investigated the electronic and elastic properties in equilibrium and under strain of the type-II Dirac semimetal NiTe$_2$ using density functional theory (DFT). Our results demonstrate the tunability of Dirac nodes' energy and momentum with strain and that it is possible to bring them closer to the Fermi level, while other metallic bands are supressed. We also derive a minimal 4-band effective model for the Dirac cones which accounts for the aforementioned strain effects by means of lattice regularization, providing an inexpensive way for further theoretical investigations and easy comparison with experiments. On an equal footing, we propose the static control of the electronic structure by intercalating alkali species into the van der Waals gap, resulting in the same effects obtained by strain-engineering and removing the requirement of in situ strain. Finally, evaluating the wavefunction's symmetry evolution as the lattice is deformed, we discuss possible consequences, such as Liftshitz transitions and the coexistence of type-I and type-II Dirac cones, thus motivating future investigations."
doi: "10.1103/PhysRevB.103.125134"
publication: "Phys. Rev. B"
authors: ["Pedro P. Ferreira", "Antonio L. R. Manesco", "Thiago T. Dorini", "Lucas E. Correa", "Gabrielle Weber", "Antonio J. S. Machado", "Luiz T. F. Eleno", ]
links:
- name: Journal link
  url: https://doi.org/10.1103/physrevb.103.125134
- name: Online talk
  url: http://youtu.be/1ZCcnwqFZsM
- name: arXiv link
  url: https://arxiv.org/abs/2006.14071
publication_types: ["2"]
math: true
highlight: true
---
