---
title: "Electronic properties of InAs/EuS/Al hybrid nanowires"
date: "2020-11-12"
abstract: "We study the electronic properties of InAs/EuS/Al heterostructures as explored in a recent experiment [S. Vaitiekenas \emph{et al.}, Nat. Phys. (2020)], combining both spectroscopic results and microscopic device simulations. In particular, we use angle-resolved photoemission spectroscopy to investigate the band bending at the InAs/EuS interface. The resulting band offset value serves as an essential input to subsequent microscopic device simulations, allowing us to map the electronic wave function distribution. We conclude that the magnetic proximity effects at the Al/EuS as well as the InAs/EuS interfaces are both essential to achieve topological superconductivity at zero applied magnetic field. Mapping the topological phase diagram as a function of gate voltages and proximity-induced exchange couplings, we show that the ferromagnetic hybrid nanowire with overlapping Al and EuS layers can become a topological superconductor within realistic parameter regimes, and that the topological phase can be optimized by external gating. Our work highlights the need for a combined experimental and theoretical effort for faithful device simulation."
doi: "10.1103/PhysRevB.104.014516"
publication: "Phys. Rev. B"
authors: ["Chun-Xiao Liu", "Sergej Schuwalow", "Yu Liu", "Kostas Vilkelis", "A. L. R. Manesco", "P. Krogstrup", "Michael Wimmer", ]
links:
- name: Journal link
  url: https://doi.org/10.1103/physrevb.104.014516
- name: Online talk
  url: http://youtu.be/G2reZGMaN_Y
- name: arXiv link
  url: https://arxiv.org/abs/2011.06567
publication_types: ["2"]
math: true
highlight: true
---
