---
title: "Correlations in the elastic Landau level of spontaneously buckled graphene"
date: "2020-03-11"
abstract: "Electronic correlations stemming from nearly flat bands in van der Waals materials have demonstrated to be a powerful playground to engineer artificial quantum matter, including superconductors, correlated insulators and topological matter. This phenomenology has been experimentally observed in a variety of twisted van der Waals materials, such as graphene and dichalcogenide multilayers. Here we show that spontaneously buckled graphene can yield a correlated state, emerging from an elastic pseudo Landau level. Our results build on top of recent experimental findings reporting that, when placed on top of hBN or NbSe$_2$ substrates, wrinkled graphene sheets relax forming a periodic, long-range buckling pattern. The low-energy physics can be accurately described by electrons in the presence of a pseudo-axial gauge field, leading to the formation of sublattice-polarized Landau levels. Moreover, we verify that the high density of states at the zeroth Landau level leads to the formation of a periodically modulated ferrimagnetic groundstate, which can be controlled by the application of external electric fields. Our results indicate that periodically strained graphene is a versatile platform to explore emergent electronic states arising from correlated elastic Landau levels."
doi: "10.1088/2053-1583/abbc5f"
publication: "2D Mater."
authors: ["Antonio L. R. Manesco", "Jose L. Lado", "Eduardo V. S. Ribeiro", "Gabrielle Weber", "Durval Rodrigues Jr", ]
links:
- name: Journal link
  url: https://doi.org/10.1088/2053-1583/abbc5f
- name: Online talk
  url: http://youtu.be/xNxuybvuuHk
- name: arXiv link
  url: https://arxiv.org/abs/2003.05163
publication_types: ["2"]
math: true
highlight: true
---
