---
title: "Effective model for Majorana modes in graphene"
date: "2019-03-03"
abstract: "It was recently proposed that the interface between a graphene nanoribbon in the canted antiferromagnetic quantum Hall state and a s-wave superconductor may present topological superconductivity, resulting in the appearance of Majorana zero modes. However, a description of the low-energy physics in terms of experimentally controllable parameters was still missing. Starting from a mean-field continuum model for graphene in proximity to a superconductor, we derive the low-energy effective Hamiltonian describing the interface of this heterojunction from first principles. A comparison between tight-binding simulations and analytical calculations with effective masses suggests that normal reflections at the interface must be considered in order to fully describe the low-energy physics."
doi: "10.1103/PhysRevB.100.125411"
publication: "Phys. Rev. B"
authors: ["A. L. R. Manesco", "D. Rodrigues Jr.", "G. Weber", ]
links:
- name: Journal link
  url: https://doi.org/10.1103/physrevb.100.125411
- name: arXiv link
  url: https://arxiv.org/abs/1903.00807
publication_types: ["2"]
math: true
highlight: true
---
