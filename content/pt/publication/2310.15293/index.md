---
title: "A ballistic electron source with magnetically-controlled valley polarization in bilayer graphene"
date: "2023-10-23"
abstract: "The achievement of valley-polarized electron currents is a cornerstone for the realization of valleytronic devices. Here, we report on ballistic coherent transport experiments where two opposite quantum point contacts (QPCs) are defined by electrostatic gating in a bilayer graphene (BLG) channel. By steering the ballistic currents with an out-of-plane magnetic field we observe two current jets, a consequence of valley-dependent trigonal warping. Tuning the BLG carrier density and number of QPC modes (m) with a gate voltage we find that the two jets are present for m=1 and up to m=6, indicating the robustness of the effect. Semiclassical simulations which account for size quantization and trigonal warping of the Fermi surface quantitatively reproduce our data without fitting parameters, confirming the origin of the signals. In addition, our model shows that the ballistic currents collected for non-zero magnetic fields are valley-polarized independently of m, but their polarization depends on the magnetic field sign, envisioning such devices as ballistic current sources with tuneable valley-polarization."
doi: "10.1103/PhysRevLett.133.156301"
publication: "Phys. Rev. Lett."
authors: ["Josep Ingla-Aynés", "Antonio L. R. Manesco", "Talieh S. Ghiasi", "Kenji Watanabe", "Takashi Taniguchi", "Herre S. J. van der Zant", ]
links:
- name: Journal link
  url: https://doi.org/10.1103/physrevlett.133.156301
- name: arXiv link
  url: https://arxiv.org/abs/2310.15293
publication_types: ["2"]
math: true
highlight: true
---
