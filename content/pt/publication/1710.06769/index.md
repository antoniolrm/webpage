---
title: "One-dimensional p-wave superconductor toy-model for Majorana fermions in multiband semiconductor nanowires"
date: "2017-10-18"
abstract: "Majorana fermions are particles identical to their antiparticles proposed theoretically in 1937 by Ettore Majorana as real solutions of the Dirac equation. Alexei Kitaev suggested that Majorana particles should emerge in condensed matter systems as zero mode excitations in one-dimensional p-wave superconductors, with possible applications in quantum computation due to their non-abelian statistics. The search for Majorana zero modes in condensed matter systems led to one of the first realistic models based in a semiconductor nanowire with high spin-orbit coupling, induced superconducting s-wave pairing and Zeeman splitting. Soon, it was realized that size-quantization effects should generate subbands in these systems that could even allow the emergence of more than one Majorana mode at each edge, resulting in a zero bias peak on the differential conductance with a different shape from the predicted by simplified theoretical models. In this work, we provide a connection between a finite-size nanowire with two occupied subbands and a 2-band Kitaev chain and discuss the advantage of an one-dimensional model to understand the phenomenology of the system, including the presence of a hidden chiral symmetry and its similarity with a spinfull Kitaev chain under a magnetic field."
doi: "10.1109/TASC.2018.2807361"
publication: "IEEE Trans. Appl. Supercond."
authors: ["Antônio Lucas Rigotti Manesco", "Gabriel Weber", "Durval Rodrigues Jr", ]
links:
- name: Journal link
  url: https://doi.org/10.1109/tasc.2018.2807361
- name: arXiv link
  url: https://arxiv.org/abs/1710.06769
publication_types: ["2"]
math: true
highlight: true
---
