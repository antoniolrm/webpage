---
title: "Development and Characterization of Cu-Nb-MgB$_2$ and CuNi- Nb-MgB$_2$ Wires With {VB$_2$ and Carbon Nanotube Additions"
date: "2015-01-06"
abstract: "The relatively high critical temperature and upper critical field and the low cost of the raw materials are the main reasons to consider MgB2 as a very promising material for superconducting applications. Improving the relatively low flux pinning in this material is important to optimize the critical current density of MgB2 superconducting wires, tape, and bulks. Adding secondary phases in a controlled way can create new pinning centers and improve the critical current density. This paper describes a methodology to produce MgB2 powders containing additions of diborides (VB2) and carbon (carbon nanotubes) that can also improve the upper critical field. MgB2 powders with these additions were used to produce Cu-Nb-MgB2 and CuNi-Nb-MgB2 multifilamentary wires. Characterization of the samples showed the microstructure, phase distribution, and microhardness in their cross sections after mechanical deformation, along with some superconducting properties and characteristics."
doi: "10.1109/TASC.2014.2387698"
publication: "IEEE Transactions on Applied Superconductivity"
authors: ["Durval Rodrigues Jr", "Luiz H. M. Antunes", "Antonio L. R. Manesco", "Eduardo M. Moraes", "Lucas B. S. da Silva", ]
links:
- name: Journal link
  url: http://dx.doi.org/10.1109/TASC.2014.2387698
publication_types: ["2"]
math: true
highlight: true
---
