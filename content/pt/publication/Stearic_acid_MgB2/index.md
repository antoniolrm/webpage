---
title: "The Influence of Stearic Acid Addition on the Superconducting Properties of MgB$_2$"
date: "2016-03-22"
abstract: "Magnesium diboride superconductors are important materials used for practical applications around the world, due to the properties and to the low price of the precursor elements, as well as the possibility to apply the material in magnets and electronic devices, operating with cryocoolers. In the present work, a methodology to optimize the properties of MgB 2 bulk superconductors is described. The method uses the addition of an organic carbon source (stearic acid, C$_18$H$_36$O$_2$), due to the homogeneous distribution of the doping material in the matrix, before final chemical reaction. The procedure was developed using high-energy ball milling to mix the precursor powders and performing heat treatment in a hot isostatic press, to improve the densification and grain connectivity. As a result, the critical current density enhanced around 600 times at 3T, when compared to the pure MgB$_2$ samples produced using the same approach."
doi: "10.1109/TASC.2016.2542289"
publication: "IEEE Transactions on Applied Superconductivity"
authors: ["Lucas B. S. da Silva", "Alan A. Vianna", "Antonio L. R. Manesco", "Eric E. Hellstrom", "Durval Rodrigues Jr", ]
links:
- name: Journal link
  url: http://dx.doi.org/10.1109/TASC.2016.2542289
publication_types: ["2"]
math: true
highlight: true
---
