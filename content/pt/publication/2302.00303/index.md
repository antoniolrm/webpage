---
title: "Specular electron focusing between gate-defined quantum point contacts in bilayer graphene"
date: "2023-02-01"
abstract: "We report on multiterminal measurements in a ballistic bilayer graphene (BLG) channel where multiple spin and valley-degenerate quantum point contacts (QPCs) are defined by electrostatic gating. By patterning QPCs of different shapes and along different crystallographic directions, we study the effect of size quantization and trigonal warping on the transverse electron focusing (TEF) spectra. Our TEF spectra show eight clear peaks with comparable amplitude and weak signatures of quantum interference at the lowest temperature, indicating that reflections at the gate-defined edges are specular and transport is phase coherent. The temperature dependence of the scattering rate indicates that electron-electron interactions play a dominant role in the charge relaxation process for electron doping and temperatures below 100 K. The achievement of specular reflection, which is expected to preserve the pseudospin information of the electron jets, is promising for the realization of ballistic interconnects for new valleytronic devices."
doi: "10.1021/acs.nanolett.3c00499"
publication: "Nano Lett."
authors: ["Josep Ingla-Aynés", "Antonio L. R. Manesco", "Talieh S. Ghiasi", "Serhii Volosheniuk", "Kenji Watanabe", "Takashi Taniguchi", "Herre S. J. van der Zant", ]
links:
- name: Journal link
  url: https://doi.org/10.1021/acs.nanolett.3c00499
- name: arXiv link
  url: https://arxiv.org/abs/2302.00303
publication_types: ["2"]
math: true
highlight: true
---
