---
title: "Hidden chiral symmetries in BDI multichannel Kitaev chains"
date: "2017-08-25"
abstract: "Realistic implementations of the Kitaev chain require, in general, the introduction of extra internal degrees of freedom. In the present work, we discuss the presence of hidden BDI symmetries for free Hamiltonians describing systems with an arbitrary number of internal degrees of freedom. We generalize results of a spinfull Kitaev chain to construct a Hamiltonian with $n$ internal degrees of freedom and obtain the corresponding hidden chiral symmetry. As an explicit application of this generalized result, we exploit by analytical and numerical calculations the case of a spinful 2-band Kitaev chain, which can host up to 4 Majorana bound states. We also observe the appearence of minigap states, when chiral symmetry is broken."
doi: "10.1088/1361-648X/aab722"
publication: "J. Phys.: Condens. Matter"
authors: ["Antônio Lucas Rigotti Manesco", "Gabriel Weber", "Durval Rodrigues Jr", ]
links:
- name: Journal link
  url: https://doi.org/10.1088/1361-648x/aab722
- name: arXiv link
  url: https://arxiv.org/abs/1708.07866
publication_types: ["2"]
math: true
highlight: true
---
