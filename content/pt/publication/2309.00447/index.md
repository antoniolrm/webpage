---
title: "Fermionic quantum computation with Cooper pair splitters"
date: "2023-09-01"
abstract: "We propose a practical implementation of a universal quantum computer that uses local fermionic modes (LFM) rather than qubits. The device layout consists of quantum dots tunnel coupled by a hybrid superconducting island and a tunable capacitive coupling between the dots. We show that coherent control of Cooper pair splitting, elastic cotunneling, and Coulomb interactions allows us to implement the universal set of quantum gates defined by Bravyi and Kitaev. Due to the similarity with charge qubits, we expect charge noise to be the main source of decoherence. For this reason, we also consider an alternative design where the quantum dots have tunable coupling to the superconductor. In this second device design, we show that there is a sweetspot for which the local fermionic modes are charge neutral, making the device insensitive to charge noise effects. Finally, we compare both designs and their experimental limitations and suggest future efforts to overcome them."
doi: "10.21468/SciPostPhys.16.5.135"
publication: "SciPost Phys."
authors: ["Kostas Vilkelis", "Antonio Manesco", "Juan Daniel Torres Luna", "Sebastian Miles", "Michael Wimmer", "Anton Akhmerov", ]
links:
- name: Journal link
  url: https://doi.org/10.21468/scipostphys.16.5.135
- name: Online talk
  url: http://youtu.be/_uUHnu1Dec0
- name: arXiv link
  url: https://arxiv.org/abs/2309.00447
publication_types: ["2"]
math: true
highlight: true
---
