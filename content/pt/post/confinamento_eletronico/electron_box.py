import kwant
import matplotlib.pyplot as plt
from matplotlib import rc
import numpy as np

rc("font", **{"family": "sans-serif", "sans-serif": ["Helvetica"]})
rc("text", usetex=True)
plt.rcParams["lines.linewidth"] = 2
plt.rcParams.update({'font.size': 20})

def psi(x, n):
    L = 10
    return np.sqrt(2 / L) * np.sin(n * np.pi * x / L)

def plot_psi(n_max = 3, L = 10):
    xs = np.linspace(0, L, 100)
    for m in range(1, n_max + 1):
        wf = psi(x = xs, n = m)
        plt.plot(xs, wf)
    plt.xlabel(r'$x$')
    plt.ylabel(r'$\psi$')
    plt.xlim(0, L)
    plt.xticks(ticks = [0, L/2, L], labels = [r'$0$', r'$L / 2$', r'$L$'])
    plt.axhline(y = 0, ls = '--', c = 'k')
    plt.show()
    
def E(n, L):
    return n**2 / L**2
    
def plot_E(n_max = 3, L_min = 1, L_max = 4):
    Ls = np.linspace(L_min, L_max, 100)
    for m in range(1, n_max + 1):
        Es = E(n = m, L = Ls)
        plt.plot(Ls, Es)
    plt.xlabel(r'$L$')
    plt.ylabel(r'$E\ \left[\frac{\hbar^2 \pi^2}{2m}\right]$')
    plt.axhline(y = 2, ls = '--', c = 'k')
    plt.xlim(L_min, L_max)
    plt.ylim(0, 5)
    plt.show()
    
    
        