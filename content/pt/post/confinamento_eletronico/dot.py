import kwant
import matplotlib.pyplot as plt
from matplotlib import rc
import numpy as np
import tinyarray

rc("font", **{"family": "sans-serif", "sans-serif": ["Helvetica"]})
rc("text", usetex=True)
plt.rcParams["lines.linewidth"] = 2

tau_0 = tinyarray.array([[1, 0], [0, 1]])
tau_1 = tinyarray.array([[0, 1], [1, 0]])
tau_2 = tinyarray.array([[0, -1j], [1j, 0]])
tau_3 = tinyarray.array([[1, 0], [0, -1]])

def make_dot(
    params
):
    # Set parameters
    a = 1
    t = 1
    
    # Define crystal structure
    if params['crystal'] == 'square':
        lat = kwant.lattice.square(a, norbs=2)
    elif params['crystal'] == 'honeycomb':
        lat = kwant.lattice.honeycomb(a, norbs=2)
        a, b = lat.sublattices
    
    # Define shape of the system
    if params['shape'] == 'square':
        def dot_shape(pos):
            x, y = pos
            return abs(x) < params['length'] and abs(y) < params['width']
    elif params['shape'] == 'disk':
        def dot_shape(pos):
            x, y = pos
            return x**2 + y**2 < params['radius']**2

    syst = kwant.Builder()
    syst[lat.shape(dot_shape, (0, 0))] = params['m'] * tau_3
    syst[lat.neighbors()] = - t * tau_0

    syst.eradicate_dangling()
    return syst


def plot_dot(syst):
    # Finalize system
    fsyst = syst.finalized()

    plt.rcParams.update({'font.size': 5})
    kwant.plot(
        syst,
        dpi = 200,
        fig_size = (2, 2),
        show = False
    )
    plt.tight_layout()
    plt.show()
        
def dos(syst, energy_subset):
    # Finalize system
    fsyst = syst.finalized()
    # Generate spectral density function
    spectrum = kwant.kpm.SpectralDensity(fsyst)
    # Refine calculation
    spectrum.add_moments(300)
    spectrum.add_vectors(50)
    
    # Calculate density of states
    density_subset = spectrum(energy_subset)
    
    return np.real(density_subset)
        
def plot_dos(syst, E_max = 1):
    # Define energy range
    energy_subset = np.linspace(-E_max, E_max, 500)
    
    dos = dos(syst, energy_subset)
    
    # Plot
    plt.rcParams.update({'font.size': 30})
    plt.plot(energy_subset, dos)
    plt.xlabel(r'$Energy\ [t]$')
    plt.ylabel(r'$DoS\ [a.u.]$')
    
def plot_ldos(syst, E = 0):
    # Finalize system
    syst = syst.finalized()
    
    # Define density operator
    kwant_op = kwant.operator.Density(syst, sum=False)
    # Calculate local spectral density
    local_dos = kwant.kpm.SpectralDensity(syst, operator=kwant_op, rng=0)
    # Refine calculation
    local_dos.add_moments(500)
    local_dos.add_vectors(30)
    
    # Calculate LDoS at energy E
    energy_ldos = local_dos(energy = E)
    
    # Plot
    plt.rcParams.update({'font.size': 30})
    kwant.plotter.density(
        syst, np.real(energy_ldos),
        cmap = 'plasma_r',
        relwidth=0.05,
        background = 'w'
    )
    plt.show()