import kwant
import matplotlib.pyplot as plt
from matplotlib import rc
import numpy as np
import tinyarray

rc("font", **{"family": "sans-serif", "sans-serif": ["Helvetica"]})
rc("text", usetex=True)
plt.rcParams["lines.linewidth"] = 2
plt.rcParams.update({'font.size': 20})

tau_0 = tinyarray.array([[1, 0], [0, 1]])
tau_1 = tinyarray.array([[0, 1], [1, 0]])
tau_2 = tinyarray.array([[0, -1j], [1j, 0]])
tau_3 = tinyarray.array([[1, 0], [0, -1]])

def make_nanowire(params):
    # Set parameters
    a = 1
    t = 1
    
    def wire(pos):
        x, y = pos
        return abs(x) < params['length'] and abs(y) < params['width']
    
    # Define crystal structure
    lat = kwant.lattice.square(a, norbs=2)

    syst = kwant.Builder()
    syst[lat.shape(wire, (0, 0))] = params['m'] * tau_3
    syst[lat.neighbors()] = - t * tau_0
    
    def wire_width(pos):
        x, y = pos
        return abs(y) < params['width']
    
    lead = kwant.Builder(kwant.TranslationalSymmetry((-a, 0)))
    lead[lat.shape(wire_width, (0, 0))] = params['m'] * tau_3
    lead[lat.neighbors()] = - t * tau_0
    
    syst.attach_lead(lead)
    syst.attach_lead(lead.reversed())
    
    return syst

def conductance(syst, energies):
    def cond(energy):
        smatrix = kwant.smatrix(
            syst,
            energy
        )
        return smatrix.transmission(1, 0)
    return list(map(cond, energies))

def plot_cond(params, energies):
    syst = make_nanowire(params).finalized()
    conds = conductance(syst, energies)
    plt.plot(energies, conds)
    plt.xlabel(r'$E\ [t]$')
    plt.ylabel(r'$G\ \left[\frac{e^2}{h}\right]$')
    plt.show()
        