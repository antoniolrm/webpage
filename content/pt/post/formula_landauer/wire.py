import kwant
from kwant.digest import uniform
import numpy as np
from cmath import exp

import matplotlib.pyplot as plt
from matplotlib import rc
rc('axes.formatter', useoffset=False)
rc('text', usetex=True)
plt.rcParams['figure.figsize'] = (6, 3)
plt.rcParams['lines.linewidth'] = .65
plt.rcParams['font.size'] = 25
plt.rcParams['legend.fontsize'] = 25

t = 1

def onsite(site, U_0, salt):
    return 4 * t + U_0 * (uniform(repr(site), repr(salt)) - 0.5)

def hopping(site1, site2, phi):
    x1, y1 = site1.pos
    x2, y2 = site2.pos
    return - t * exp(- 0.5j * phi * (x1 - x2) * (y1 + y2))

def create_sys(L = 20, W = 10):
    lat = kwant.lattice.square(a = 1)
    sys = kwant.Builder()

    sys[(lat(x, y) for x in range(L) for y in range(W))] = onsite
    sys[lat.neighbors()] = hopping

    lead = kwant.Builder(kwant.TranslationalSymmetry([-1, 0]))
    lead[(lat(0, y) for y in range(W))] = onsite
    lead[lat.neighbors()] = hopping

    sys.attach_lead(lead)
    sys.attach_lead(lead.reversed())

    sys = sys.finalized()

    return sys

def compute_conductance(energy, sys, phi = 0, U_0 = 0, salt = 10):
    params = dict(phi = phi, U_0 = U_0, salt = salt)
    s_matrix = kwant.smatrix(sys, energy = energy, params = params)
    G = s_matrix.transmission(1, 0)

    return G

def plot_cond(par, Gs, par2 = None, Gs2 = None, x_label = r'$eV [t]$', y_label = r'$G\ \left[\frac{e^2}{h}\right]$', ylim = None):
    plt.plot(par, Gs, c = 'k', lw = 2)
    if Gs2:
        plt.plot(par2, Gs2, c = 'r', lw = 2)
    plt.ylabel(r'$G\ \left[\frac{e^2}{h}\right]$')
    plt.xlabel(x_label)
    if ylim:
        plt.ylim(ylim[0], ylim[1])
    plt.show()
