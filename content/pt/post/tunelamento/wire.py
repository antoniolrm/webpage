import kwant
from kwant.digest import uniform
import numpy as np
from cmath import exp

import matplotlib.pyplot as plt
from matplotlib import rc
rc('axes.formatter', useoffset=False)
rc('text', usetex=True)
plt.rcParams['figure.figsize'] = (6, 3)
plt.rcParams['lines.linewidth'] = .65
plt.rcParams['font.size'] = 25
plt.rcParams['legend.fontsize'] = 25

t = 1

def create_sys(L = 100, W = 10, barrier = 'single'):
    lat = kwant.lattice.square(a = 1)
    sys = kwant.Builder()

    def shape(pos, L = L, W = W):
        x, y = pos
        return (- L / 2 < x < L / 2) and (- W / 2 < y < W / 2)
    
    def shape_lead(pos, W = W):
        x, y = pos
        return (- W / 2 < y < W / 2)

    def single_barrier(x, l):
        return np.heaviside(x + l, 0) * np.heaviside(l - x, 0)

    def double_barrier(x, l):
        return single_barrier(x + L / 8, l) + single_barrier(x - L / 8, l)

    def onsite(site, U, l):
        x, y = site.pos
        if barrier == 'single':
            barrier_pot = single_barrier(x, l)
        elif barrier == 'double':
            barrier_pot = double_barrier(x, l)
        return 4 * t + U * barrier_pot
    
    sys[lat.shape(shape, (0, 0))] = onsite
    sys[lat.neighbors()] = - t

    lead = kwant.Builder(kwant.TranslationalSymmetry((-1, 0)))
    lead[lat.shape(shape_lead, (0, 0))] = 4 * t
    lead[lat.neighbors()] = - t

    sys.attach_lead(lead)
    sys.attach_lead(lead.reversed())

    sys = sys.finalized()

    return sys

def compute_conductance(U, l, sys, energy = 0.2):
    params = dict(U = U, l = l)
    s_matrix = kwant.smatrix(sys, energy = energy, params = params)
    G = s_matrix.transmission(1, 0) / s_matrix.submatrix(0, 0).shape[0]

    return G

def plot_cond(par, Gs, par2 = None, Gs2 = None, x_label = r'$eV\ [t]$', y_label = r'$G\ \left[\frac{e^2}{h}\right]$', ylim = None, show=True):
    plt.plot(par, Gs, c = 'k', lw = 2)
    if Gs2:
        plt.plot(par2, Gs2, c = 'r', lw = 2)
    plt.ylabel(y_label)
    plt.xlabel(x_label)
    if ylim:
        plt.ylim(ylim[0], ylim[1])
    if show:
        plt.show()
