jupyter nbconvert notebook.ipynb --to markdown --NbConvertApp.output_files_dir=. --no-input
# Copy contents of header.md and append to index.md
cat header.txt | tee index.md

# Copy the contents of notebook.md and append it to index.md:
cat notebook.md | tee -a index.md

# Remove the temporary file:
rm -f notebook.md
