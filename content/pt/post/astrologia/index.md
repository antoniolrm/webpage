---
title: '"Meu mapa astral sempre acerta! Não pode ser coincidência!"'
subtitle: ''
summary: []
authors:
- antonio
tags: [intermediário]
categories: []
date: "2020-09-25"
lastmod: "2020-09-25"
featured: false
draft: false

projects: []
---

Acompanhei uma discussão que surgiu ontem (23/09/2020) no Twitter, por conta de uma postagem da [@stephanevw](https://twitter.com/stephanevw) que argumentava sobre a ausência de evidências que embasem astrologia. Naturalmente, nós vemos mais uma vez quem milita pelo fim da pseudociência defendendo a pseudociência de estimação. O texto, ao contrário do que talvez você espere por ler até aqui, não é nem de longe um embasamento filosófico sobre pseudociência ou metodologia científica, mas uma curiosidade minha ao ler o "argumento" de um dos defensores de astrologia que dizia não ser possível um cientista tentar tirar credibilidade da astrologia, sendo que ela sempre funciona. Resolvi desenferrujar meus conhecimentos de probabilidade e estatística pra entender se é possível chegar perto de 100% de acerto usando apenas dados totalmente aleatórios.

## O início da discussão

Deixe-me apresentar como fiz o teste. Vamos supor que você, cético, esteja conversando com um crente em astrologia, que tenta te convencer sobre a boa capacidade de previsão com um teste muito simples:
1. faça uma lista com várias características suas, cujas respostas sejam sim ou não;
2. compare com o seu mapa astral;
3. acredite ou não em atrologia baseado no resultado.

Para ilustrar, pense que seu conhecido crente te deu a seguinte lista:

| Característica | Sim | Não |
|:--------------:|:---:|:---:|
| Esfomeado      |  x  |     |
| Impaciente     |     |  x  |
| Ciumento       |     |  x  |
| Estudioso      |     |  x  |
| Emotivo        |  x  |     |

E aí vocês começam a comparar com o que era esperado para o seu signo:

| Característica | Sim | Não | Signo acertou? |
|:--------------:|:---:|:---:|:--------------:|
| Esfomeado      |  x  |     |                |
| Impaciente     |     |  x  |        x       |
| Ciumento       |     |  x  |                |
| Estudioso      |     |  x  |                |
| Emotivo        |  x  |     |                |

Não foi muito bem, apenas 1 acerto, mas "é que não levamos em conta o seu ascendente", disse o crente. Ao fazer isso:

| Característica | Sim | Não | Signo acertou? | Ascendente acertou? |
|:--------------:|:---:|:---:|:--------------:|:-------------------:|
| Esfomeado      |  x  |     |                |                     |
| Impaciente     |     |  x  |       x        |                     |
| Ciumento       |     |  x  |                |           x         |
| Estudioso      |     |  x  |                |           x         |
| Emotivo        |  x  |     |                |           x         |

Bem melhor: com o signo e o ascendente juntos, já foram 4 acertos! E então ele diz: "isso que ainda faltou considerar a sua lua". Fazem isso é voilá:

| Característica | Sim | Não | Signo acertou? | Ascendente acertou? | Lua acertou? |
|:--------------:|:---:|:---:|:--------------:|:-------------------:|:------------:|
| Esfomeado      |  x  |     |                |                     |       x      |
| Impaciente     |     |  x  |       x        |                     |       x      |
| Ciumento       |     |  x  |                |          x          |              |
| Estudioso      |     |  x  |                |          x          |       x      |
| Emotivo        |  x  |     |                |          x          |              |

Não é que seu mapa astral acertou tudo? Então mapa astral funciona? Não é bem assim...

## O modelo

### Viu só!

Então você pensa: "mas não é possível!" E começa a imaginar como fazer um teste mais complicado. Você junta todos os seus colegas de turma da faculdade/escola (por exemplo, você juntou 100 deles). E montou uma lista não com 5, mas com 50 características! Então, você disse: se o mapa astral acertar pelo menos 40 características de 80 dessas pessoas, eu começo a acreditar em astrologia. Parece um teste bem difícil de passar, né? Mas não é.

Vamos falar um pouco de estatística, então. Vamos supor que eu preencha a terceira, a quarta e a quinta colunas de forma **completamente aleatória**, que seria o equivalente a eu inventar absolutamente tudo sobre o seu signo, o seu ascendente e a sua lua. Começando só pelo signo, a probablidade de acertar pelo menos $n$ de $N$ características é, sabendo que a probabilidade de acertar uma delas é $1/2$ (sim ou não):
$$
\sum_{m = n}^N \left( \frac12 \right)^m \left( \frac12 \right)^{N-m} \frac{m! (N-m)!}{N!}
$$
eu sei, não é uma fórmula muito simples. Mas ninguém disse que seria.

Ou seja, sendo 50 caracteŕisicas, a probabilidade de acertar pelo menos 40 delas é
$$
\left( \frac12 \right)^{40} \left( \frac12 \right)^{50-40} \frac{40! (50-40)!}{50!} = 0.00119\\%.
$$
Em outras palavras, é bem difícil acertar tudo apenas considerado seu signo. "Viu só!", disse o astrólogo. Quase impossível acertar tantas características inventando um signo.

### Não tão cedo!

Mas e se levarmos em conta o ascendente e a lua, aquilo que era complicado fica mais ainda. Não vou encher de fórmulas, mas você pode encontrar o código que eu usei para fazer as contas [aqui](https://gitlab.com/antoniolrm/webpage/-/blob/master/content/pt/post/astrologia/notebook.ipynb). Vou me limitar apenas ao resultado: no gráfico abaixo, você pode ver qual as probabilidades de acertar pelo menos $n$ características, usando somente o signo.

![png](./notebook_6_0.png)

Como esperado, chutando "sim" ou "não", as chances de acertar metade (25) das carcterísticas é bem alto, mas essa probabilidade cai muito se exigirmos um número de acertos maior que 25, e é quase zero para 40 acertos, como já disse anteriormente.

Quando adicionamos o ascendente, veja só o que acontece:

![png](./notebook_8_0.png)

As nossas chances de acertar mais características aumenta. Veja que o degrau que antes estava centrado em 25, agora está perto de 35. Quanto mais esse degrau se desloca para a direita, mais fácil fica de acertar um número $n$ de características. De fato, a probabilidade de acertar pelo menos 40 das caracterísitcas subiu de $0.00119 \\% $ para $26.22 \\% $!

E se levarmos a lua em conta? Veja com seus próprios olhos:

![png](./notebook_10_0.png)

Note o quanto o degrau se moveu para a direita. Agora, as chances de acertar pelo menos 40 das 50 características subiu de $0.00119\\%$ para $95.79\\%$!!!! Ou seja, de um grupo de 100 pessoas, é esperado que 95 delas passe no teste. Naturalmete, adicionando mais e mais parâmetros, a tendência do gráfico é se mover cada vez mais para a esquerda. Quanto mais completo for o mapa astral, mesmo que seja completamente aleatório, as chances de predizerem todas as características da lista é maior. Compare os três gráficos juntos abaixo (azul: somente signo; laranja: signo e ascendente; verde: signo, ascendente e lua).

![png](./featured.png)

"Meu mapa astral sempre acerta! Não pode ser coincidência!"

Na verdade, pode. E baseado na ausência de evidências, provavelmente é.
