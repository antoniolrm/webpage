import yaml
from github import Github
g=Github()
repo=g.get_repo('virtualscienceforum/virtualscienceforum')

with open('./publications_arxiv.yml') as f:
    data_website = yaml.load(f, Loader=yaml.FullLoader)
talks = repo.get_contents(
    'talks.yml'
).decoded_content.decode()
data_vsf = yaml.load(talks, Loader=yaml.FullLoader)

ids = []
for entry in data_website:
    ids.append(entry['id'])

for entry in data_vsf:
    if 'preprint' in entry:
        if entry['preprint'] in ids:
            if 'youtube_id' in entry:
                idx = ids.index(entry['preprint'])
                data_website[idx]['youtube_id'] = entry['youtube_id']

with open(r'./publications_arxiv.yml', 'w') as file:
    documents = yaml.dump(data_website, file)
