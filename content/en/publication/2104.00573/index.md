---
title: "Correlation-induced valley topology in buckled graphene superlattices"
date: "2021-04-01"
abstract: "Flat bands emerging in buckled monolayer graphene superlattices have been recently shown to realize correlated states analogous to those observed in twisted graphene multilayers. Here, we demonstrate the emergence of valley topology driven by competing electronic correlations in buckled graphene superlattices. We show, both by means of atomistic models and a low-energy description, that the existence of long-range electronic correlations leads to a competition between antiferromagnetic and charge density wave instabilities, that can be controlled by means of screening engineering. Interestingly, we find that the emergent charge density wave has a topologically non-trivial electronic structure, leading to a coexistent quantum valley Hall insulating state. In a similar fashion, the antiferromagnetic phase realizes a spin-polarized quantum valley-Hall insulating state. Our results put forward buckled graphene superlattices as a new platform to realize interaction-induced topological matter."
doi: "10.1088/2053-1583/ac0b48"
publication: "2D Mater."
authors: ["Antonio L. R. Manesco", "Jose L. Lado", ]
links:
- name: Journal link
  url: https://doi.org/10.1088/2053-1583/ac0b48
- name: Online talk
  url: http://youtu.be/92gahnZ920g
- name: arXiv link
  url: https://arxiv.org/abs/2104.00573
publication_types: ["2"]
math: true
highlight: true
---
