---
title: "Strong Electronic Interaction and Signatures of Nodal Superconductivity in Zr$_5$Pt$_3$C$_x$"
date: "2018-09-13"
abstract: "The physical properties of the Zr$_5$Pt$_3$ compound with interstitial carbon in hexagonal D8$_8$-structure was investigated. A set of macroscopic measurements reveal a bulk superconducting at approximately 7 K for Zr$_5$Pt$_3$C$_{0.3}$ close to Zr$_5$Pt$_3$, also with a correlate anomalous resistivity behavior. However, both the signatures of strong electron-electron interaction, and the electronic contribution to specific heat, increase dramatically with the C doping. For the first time the x-ray photoelectron spectra compared with DFT/PWLO calculations of electronic structure show a complex Fermi surface with high density of states for Zr$_5$Pt$_3$. Also results show the signature of unconventional superconductivity. Indeed, was observed an unusual behavior for lower and upper critical field diagrams of Zr$_5$Pt$_3$C$_{0.3}$. The temperature dependence of penetration length and electronic contribution to specific heat suggests that electronic pairing deviates of $s$-wave the BCS scenario."
authors: ["S. T. Renosto", "R. Lang", "A. L. R. Manesco", "D. Rodrigues Jr.", "F. B. Santos", "A. J. S. Machado", "M. R. Baldan", "E. Diez", ]
links:
- name: arXiv link
  url: https://arxiv.org/abs/1809.05008
publication_types: ["2"]
math: true
highlight: true
---
