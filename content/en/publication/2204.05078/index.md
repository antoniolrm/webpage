---
title: "Near zero-energy Caroli-de Gennes-Matricon vortex states in the presence of impurities"
date: "2022-04-11"
abstract: "Caroli-de Gennes-Matricon (CdGM) states are localized states with a discrete energy spectrum bound to the core of vortices in superconductors. In topological superconductors, CdGM states are predicted to coexist with zero-energy, chargeless states widely known as Majorana zero modes (MZMs). Due to their energy difference, current experiments rely on scanning tunneling spectroscopy methods to distinguish between them. This work shows that electrostatic inhomogeneities can push trivial CdGM states arbitrarily close to zero energy in non-topological systems where no MZM is present. Furthermore, the BCS charge of CdGM states is suppressed under the same mechanism. Through exploration of the impurity parameter space, we establish that these two phenomena generally happen in consonance. Our results show that energy and charge shifts in CdGM may be enough to imitate the spectroscopic signatures of MZMs even in cases where the estimated CdGM level spacing (in the absence of impurities) is much larger than the typical experimental level broadening."
doi: "10.1103/PhysRevB.107.184509"
publication: "Phys. Rev. B"
authors: ["Bruna S. de Mendonça", "Antonio L. R. Manesco", "Nancy Sandler", "Luis G. G. V. Dias da Silva", ]
links:
- name: Journal link
  url: https://doi.org/10.1103/physrevb.107.184509
- name: Online talk
  url: http://youtu.be/WE_sxbXYdYM
- name: arXiv link
  url: https://arxiv.org/abs/2204.05078
publication_types: ["2"]
math: true
highlight: true
---
