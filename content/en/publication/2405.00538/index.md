---
title: "Probing valley phenomena with gate-defined valley splitters"
date: "2024-05-01"
abstract: "Despite many reports of valley-related phenomena in graphene and its multilayers, current transport experiments cannot probe valley phenomena without the application of external fields. Here we propose a gate-defined valley splitter as a direct transport probe for valley phenomenon in graphene multilayers. First, we show how the device works, its magnetotransport response, and its robustness against fabrication errors. Secondly, we present two applications for valley splitters: (i) resonant tunneling of quantum dots probed by a valley splitter shows the valley polarization of dot levels; (ii) a combination of two valley splitters resolves the nature of order parameters in mesoscopic samples."
doi: "10.21468/SciPostPhys.18.2.062"
publication: "SciPost Phys."
authors: ["Juan Daniel Torres Luna", "Kostas Vilkelis", "Antonio L. R. Manesco", ]
links:
- name: Journal link
  url: https://doi.org/10.21468/scipostphys.18.2.062
- name: Online talk
  url: http://youtu.be/424oTxtFQNU
- name: arXiv link
  url: https://arxiv.org/abs/2405.00538
publication_types: ["2"]
math: true
highlight: true
---
