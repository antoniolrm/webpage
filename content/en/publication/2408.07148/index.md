---
title: "Gate-defined Kondo lattices with valley-helical quantum dot arrays"
date: "2024-08-13"
abstract: "Kondo physics and heavy-fermion behavior have been predicted and observed in moir\'e materials. The electric tunability of moir\'e materials allows an in-situ study of Kondo lattices' phase diagrams, which is not possible with their intermetallic counterparts. However, moir\'e platforms rely on twisting, which introduces twisting angle disorder and undesired buckling. Here we propose device layouts for one- and two-dimensional gate-defined superlattices in Bernal bilayer graphene where localized states couple to dispersive valley-helical modes. We show that, under electronic interactions, these superlattices are described by an electrically tunable Kondo-Heisenberg model."
authors: ["Antonio L. R. Manesco", ]
links:
- name: arXiv link
  url: https://arxiv.org/abs/2408.07148
publication_types: ["2"]
math: true
highlight: true
---
