---
title: "Spatial separation of spin currents in transition metal dichalcogenides"
date: "2022-06-15"
abstract: "We theoretically predict spatial separation of spin-polarized ballistic currents in transition metal dichalcogenides (TMDs) due to trigonal warping. We quantify the effect in terms of spin polarization of charge carrier currents in a prototypical 3-terminal ballistic device where spin-up and spin-down charge carriers are collected by different leads. We show that the magnitude of the current spin polarization depends strongly on the charge carrier energy and the direction with respect to crystallographic orientations in the device. We study the (negative) effect of lattice imperfections and disorder on the observed spin polarization. Our investigation provides an avenue towards observing spin discrimination in a defect-free time reversal-invariant material."
doi: "10.21468/SciPostPhysCore.6.2.036"
publication: "SciPost Phys. Core"
authors: ["Antonio L. R. Manesco", "Artem Pulkin", ]
links:
- name: Journal link
  url: https://doi.org/10.21468/scipostphyscore.6.2.036
- name: arXiv link
  url: https://arxiv.org/abs/2206.07333
publication_types: ["2"]
math: true
highlight: true
---
