---
title: "Properties and superconductivity in Ti-doped NiTe2 single crystals"
date: "2018-09-04"
abstract: "Transition metal dichalcogenides (TMDs) usually show simple structures, however, with interesting properties. Recently some TMDs have been pointed out as type-II Dirac semimetals. In the present work, we investigate the physical properties of a new candidate for type-II Dirac semimetal and investigate the effect of titanium doping on physical properties of Ti-doped single crystalline samples of NiTe2. It was found that this compound shows a superconducting properties with a critical temperature close to 4.0 K. Interestingly, applied pressures up to 1.3 GPa have no effect upon the superconducting state. Density Functional Theory (DFT) calculations demonstrate the presence of a Dirac cone in the band structure of NiTe2 literature when Spin-Orbit Coupling (SOC) is included, which is in agreement with a recent report for this compound. Also, our calculations demonstrate that Ti suppresses the formation of these non-trivial states."
doi: "10.1016/j.ssc.2018.08.014"
publication: "Solid State Communications"
authors: ["B. S. de Lima", "R. R. de Cassia", "F. B. Santosa", "L. E. Correa", "T. W. Grant", "A. L. R. Manesco", "G. W. Martins L. T. F. Eleno", "M. S. Torikachvili", "A. J. S. Machado", ]
links:
- name: Journal link
  url: https://doi.org/10.1016/j.ssc.2018.08.014
- name: arXiv link
  url: https://arxiv.org/abs/1809.00753
publication_types: ["2"]
math: true
highlight: true
---
