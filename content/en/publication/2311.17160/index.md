---
title: "Chiral adiabatic transmission protected by Fermi surface topology"
date: "2023-11-28"
abstract: "We demonstrate that Andreev modes that propagate along a transparent Josephson junction have a perfect transmission at the point where three junctions meet. The chirality and the number of quantized transmission channels is determined by the topology of the Fermi surface and the vorticity of the superconducting phase differences at the trijunction. We explain this chiral adiabatic transmission (CAT) as a consequence of the adiabatic evolution of the scattering modes both in momentum and real space. The dispersion relation of the junction then separates the scattering trajectories by introducing inaccesible regions of phase space. We expect that CAT is observable in nonlocal conductance and thermal transport measurements. Furthermore, because it does not rely on particle-hole symmetry, CAT is also possible to observe directly in metamaterials."
authors: ["Isidora Araya Day", "Kostas Vilkelis", "Antonio L. R. Manesco", "A. Mert Bozkurt", "Valla Fatemi", "Anton R. Akhmerov", ]
links:
- name: Online talk
  url: http://youtu.be/OFBGtmzKjuI
- name: arXiv link
  url: https://arxiv.org/abs/2311.17160
publication_types: ["2"]
math: true
highlight: true
---
