---
# Display name
name: Antonio Manesco

# Username (this should match the folder name)
authors:
- antoniolrm

# Is this the primary user of the site?
superuser: true

# Role/position
role: Postdoctoral Researcher

# Organizations/Affiliations
organizations:
- name: Delft University of Technology
  url: "https://www.tudelft.nl/"

# Short bio (displayed in user profile at end of posts)
bio: I am a condensed matter theorist interested in nanodevices.

interests:
- Graphene and other 2D materials
- Mesoscopic superconductivity
- Nanoelectronics
- Topological insulators and superconductors

education:
  courses:
  - course: Doctor in Materials Engineering
    institution: University of São Paulo
    year: 2021
  - course: Bachelor in Engineering Physics
    institution: University of São Paulo
    year: 2016

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: "mailto:hello@antoniomanesco.org"  # For a direct email link, use "mailto:test@example.org".
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/AntonioManesco
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com.br/citations?user=NnwQ6j0AAAAJ&hl=pt-BR
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/antoniolrm
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: CV/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "hello@antoniomanesco.org"

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.  
user_groups:
- Researchers
- Visitors
---
I am a postdoctoral researcher at the Delft University of Technology, in the [Quantum Tinkerer](https://quantumtinkerer.tudelft.nl/) group. My research interest is in Condensed Matter Physics, mainly focused in electronic properties of materials and electronic transport in mesoscopic systems.
