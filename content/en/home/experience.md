+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "Experience"
subtitle = ""

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Postdoctoral researcher"
  company = "Delft University of Technology"
  company_url = "https://www.tudelft.nl/"
  location = "Delft - Netherlands"
  date_start = "2021-07-01"
  date_end = ""
  description = """
  [Quantum Tinkerer group](https://quantumtinkerer.tudelft.nl/).
  Responsibilities include:

  * Scientific research
  * Supervision of students
  """

[[experience]]
  title = "Visiting researcher"
  company = "Cornell University"
  company_url = "https://www.cornell.edu/"
  location = "Ithaca, NY - USA"
  date_start = "2022-09-05"
  date_end = "2022-09-30"
  description = """
  Visitor at [Fatemi lab](https://fatemilab.aep.cornell.edu/).
  Responsibilities include:

  * Scientific research
  """

[[experience]]
  title = "Doctorate researcher"
  company = "University of São Paulo"
  company_url = "https://www5.usp.br/"
  location = "Lorena - São Paulo - Brazil"
  date_start = "2016-08-01"
  date_end = "2021-06-01"
  description = """
  Responsibilities include:

  * Scientific research
  * Supervision of bachelor projects
  * Teaching
  """

[[experience]]
  title = "Visiting researcher"
  company = "Delft University of Technology"
  company_url = "https://www.tudelft.nl/"
  location = "Delft - Netherlands"
  date_start = "2019-09-01"
  date_end = "2020-09-01"
  description = """
  Visitor at [Quantum Tinkerer group](https://quantumtinkerer.tudelft.nl/).
  Responsibilities include:

  * Scientific research
  * Supervision of bachelor projects
  """

[[experience]]
  title = "Undergrad researcher"
  company = "University of São Paulo"
  company_url = "https://www5.usp.br/"
  location = "Lorena - São Paulo - Brazil"
  date_start = "2013-08-01"
  date_end = "2016-07-30"
  description = "Scientific research"

+++
