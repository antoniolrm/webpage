rm -f content/publications.yml content/publications_arxiv.yml
publist content/publications_arxiv.yml -a manesco_a_1 --email antoniolrm@usp.br -u
cd content
python update_vsf_videos.py
cat publications_arxiv.yml >> publications.yml
cat publications_no_preprint.yml >> publications.yml
python publist_to_md.py
cd ../static/CV
python update_bib.py
cd ..