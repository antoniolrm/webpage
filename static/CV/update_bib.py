from yaml2bib import yaml2bib
import yaml

with open("../../content/publications.yml", "r") as stream:
    publs = yaml.load(stream=stream, Loader=yaml.Loader)

pub_dict = {}

for entry in publs:
    try:
        pub_dict[entry['id']] = entry['doi']
    except:
        if entry['id'] == '1809.05008':
            pass
        else:
            arxiv_doi = '10.48550/arXiv.' + entry['id']
            pub_dict[entry['id']] = arxiv_doi

with open('publications.yml', 'w') as outfile:
    yaml.dump(pub_dict, outfile, default_flow_style=False)

yaml2bib(
    bib_fname="publications.bib",
    dois_yaml="publications.yml",
    replacements_yaml=None,
    static_bib=None,
    email='anonymous',
    doi2bib_database=None,
    crossref_database=None
)