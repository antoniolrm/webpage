---
bibliography:
- publications.bib
nocite: "[@*]"
title: Curriculum Vitae
---

# Currently

Employing computational methods to study electronic properties of
quantum materials and devices.

# Expertise

Computational condensed matter physics, topological materials,
mesoscopic physics, two-dimensional materials, research code
development.

# Education

* **2016 - 2021** Honors Doctorate (without master title) - Universidade de São Paulo
  - Thesis title: [*Correlations and topology in hybrid graphene-based devices*](https://www.teses.usp.br/teses/disponiveis/97/97134/tde-04052022-112041/en.php)
* **2012 - 2016** Bachelor in Engineering Physics - Universidade de São Paulo
  - First Class Honours

# Experience

## Research

* **2021 - ongoing** Postodoctoral researcher @ Quantum Tinkerer group - Delft University of Technology
  - Conducting independent research on realistic simulations of quantum materials and devices.
  - Supervision of bachelor, master, and doctorate students. 
  - Curator of website and outreach issues of the Quantum Tinkerer group.
* **Summer 2022**  Visiting researcher @ Fatemi lab - Cornell University
  - Developed simulations to guide the design of ongoing experiments in the Fatemi lab.
* **2016 - 2021** Doctoral researcher - University of São Paulo
  - Developed researcher on graphene-based devices.
  - My research provided theoretical explanation to Andreev phenomena in quantum Hall graphene.
  - I predicted the existence of correlated topological phenomena in strained graphene devices, which was already partially confirmed experimentally.
  - Supervised bachelor and master students.
  - Worked as a teaching assistant.
* **2019 - 2021** Visiting researcher @ Quantum Tinkerer group - Delft University of Technology
  - Developed theoretical research on nonlocal transport in proximitized quantum Hall graphene.
  - Supervised bachelor and master students in the Quantum Tinkerer group.
* **2013 - 2016** Undergraduate researcher - University of São Paulo
  - 2013 - 2014 - Preparation and characterization of superconducting samples.
  - 2014 - 2015 - Electronic structure (tight-binding and DFT) calculations of graphene.
  - DFT calculations of electron and phonon spectrum of superconducting materials.

## Teaching
* **2018/2** Teaching assistant - Linear Algebra - University of São Paulo
* **2018/1** Teaching assistant - Statistical Physics - University of São Paulo
* **2015/1** Teaching assistant - Quantum Mechanics - University of São Paulo

## Supervisions

* **2023 - 2023** Emma Nielen - Delft University of Technology
  - Bachelor thesis: *Mega scars: quantum scars in bilayer graphene*
* **2020 - 2021** Katya Fouka - Leiden University
  - Master thesis: [*Can we employ disorder to filter topological Quantum Spin Hall Effect edge states?*](https://studenttheses.universiteitleiden.nl/handle/1887/3213026)
* **2019 - 2020** Gillard Franken - Delft University of Technology
  - Bachelor thesis: *Tunneling barrier strength dependency of zero bias conductance in a multi-subband Majorana quantum wire with and without inter-band communication*
* **2019 - 2020** Eduardo Ribeiro - University of São Paulo
  - Research internship: *Generation of valley currents in strained quantum Hall graphene*
* **2016 - 2017** Murilo Bigoto - University of São Paulo
  - Undergrad research project: *Simulations of a Kitaev chain and Majorana zero modes*

# Awards and grants
* **Summer 2022** [Global Quantum Leap exchange funding](https://www.globalquantumleap.org/adhoc-22-students/antonio-manesco)
* **2016 - 2021** Doctorate fellowship - São Paulo Research Foundation (FAPESP)
* **2019 - 2020** Research Internship Abroad - São Paulo Research Foundation (FAPESP)
* **2018 - 2018** Teaching assistant - Universidade de São Paulo Educational Support Program
* **2016** Quantum Design International Award for first class student
* **2014 - 2016** Undergraduate reasearch fellowship - São Paulo Research Foundation (FAPESP)
* **2005** Municipal Mathematics Olympiad - Gold award - Jacareí - SP, Brazil

# Science outreach and event organization

* **Participation in podcasts**
  - **Mamucast S01E04** -  E o grafeno, ein? (in Portuguese)
  - **Mamucast S2E12** - Vida Acadêmica, a Universidade e tudo mais! (in Portuguese)
* **Summer 2023** Summer School in Science Communication - Leiden University
* **May 2023** Pint of Science - Den Haag
* **2021 - ongoing** Quantum Tinkerer website and outreach curator
  - Update and mantain the group's website and Twitter page.
  - Handle other outreach activities.
* **2021** [Andreev reflection in quantum Hall systems: 2021 state of the union - Virtual Science Forum](https://virtualscienceforum.org/arqh) (Instructor)
* **2021** [Mini-workshop: Introduction to computational quantum transport with Kwant - Virtual Science Forum](https://virtualscienceforum.org/quantum-transport-workshop) (Instructor)
* **2015 - 2019** [Personal blog](https://antoniomanesco.org/pt/post/)
  - Blog about Physics (in Portuguese).
* **2015 - 2019** umengenheirofisico (now offline)
  - Blog about Engineering Physics and Physics (in Portuguese).
* **2016** Brazilian Symposium in Engineering Physics (Member of the organization committee).
* **2014 - 2016** Engineering Physics Week - Universidade de São Paulo (Member of the organization committee).
* **2014--2016** $\gamma$ na Engenharia Física - Universidade de São Paulo
  - Organization of seminars from students to students on the Engineering Physics bachelor program.
* **2013 - 2014** Física WTF (now offline)
  - Blog about Physics.

## Extracurricular

* **2018 - 2019** Organizer of the Journal Club in Condensed Matter Theory - Universidady of São Paulo
* **2013 - 2015** Representative student in the
Comission for the Organization of the Engineering Physics bachelor program - Universidade de São Paulo
  - Decide courses syllabus.
  - Connect students with course organization.
  - Deal with students requests.
  - Prepare evaluation reports.
  - Interact with the federal evaluation committee.
* **2014 - 2015** Director of academic issues of the Engineering Physics Student Academic Center - Universidade de São Paulo
  - Organize academic events.
  - Comunicate with the Comission for the Organization of the Engineering Physics bachelor program.

# Skills

-   **Theory**: tight-binding, density functional theory (DFT), quantum
    transport, superconductivity.

-   **Coding:** Python (numpy, scipy, kwant, sympy, xarray, matplotlib,
    dask), bash, Quantum ESPRESSO.

-   **Soft**: project supervision, communication (presentating, writing,
    dissemination of scientific results).

# Languages
* Portuguese - native proficiency
* English - full professional proficiency
* Dutch - A2 level
* Spanish - Introductory level

# Coding projects
* [pyDACP](https://gitlab.kwant-project.org/qt/pyDACP): A python package to compute eigenvalues of using the dual applications of Chebyshev polynomials algorithm. The algorithm is described in [SciPost Phys. 11, 103 (2021)](https://scipost.org/SciPostPhys.11.6.103).